# FizzBuzz:
#   ○ Написать программу, которая выводит на экран числа от 1 до 100.
#  При этом вместо чисел, кратных трем, программа должна выводить слово «Fizz»,
#     а вместо чисел, кратных пяти — слово «Buzz». Если число кратно и 3, и 5,
#    то программа должна выводить слово «FizzBuzz».
for i in range(1, 101):
    if i % 3 == 0 and i % 5 == 0:
        print('FizzBuss ')
    elif i % 3 == 0 :
        print('Fizz')
    elif i % 5 == 0 :
        print('Buzz')
    else:
        print (i)


#• Ключ-значение :
#    ○ Написать программу, которая берет словарь и меняет местами ключи и значения.
#     Попытаться реализовать за наименьшее кол-во строк.
#        § Пример: {'key1': 'value1', 'key2': 'value2'} -> {'value1': 'key1', 'value2': 'key2'}

list1 = {'key1': 'value1', 'key2': 'value2'}
list2 = {v: k for k, v in list1.items()} #Новый словарь


#• Дубликаты:
#    ○ Написать программу, которая берет исходный список и формирует новый список.
#     В новом списке должны содержаться все элементы из исходного, за исключением дублей.
#        § Пример: [1, 1, 2, 3, 5,  4,5, 5, 6] -> [1, 2, 3, 5, 4, 6]

list3= [1, 1, 2, 3, 5,  4, 5, 5, 6]
new_list = []
for i in list3:
    if i not in new_list:
       new_list.append(i)
print(new_list)